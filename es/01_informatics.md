# ¿Qué es la Informática? {-}

¿Qué es la informática? ¿Por qué algunos le llaman «Ciencias de la
Computación»[^1]?  ¿Por qué quienes programan son incapaces de hacer bien su
trabajo mientras que la ingeniería civil no falla al diseñar un puente?

Dada la ingente cantidad de computadoras a nuestro alrededor, se piensa en la
Informática como un campo muy avanzado de la tecnología, un campo que siempre
está en la frontera del conocimiento humano. Se piensa que programar es una
habilidad especializada, únicamente necesaria si se pretende seguir una carrera
profesional concreta.

Nada más lejos de la realidad.

[^1]: Del inglés *Computer Science*.

## Un poco de historia {-}

En 1957, Karl Steinbuch[^karl-steinbuch] acuña el término «*Informatik*» en su
ensayo «*Informatik: Automatische Informationsverarbeitung*», «Informática:
Procesado de información automático».

En marzo de 1962, Philippe Dreyfus[^philipe-dreyfus] utiliza por primera vez
el término «*Informatique*» para denominar a su nueva compañía: *Société
d’informatique appliquée*.

El mismo mes, Walter Bauer funda la empresa estadounidense «*Informatics
Inc.*», registra su marca y toma medidas jurídicas contra las universidades que
utilizan la palabra para referirse al nuevo campo de estudio, forzándolas a
renombrarlo a «*Computer Science*», Ciencias de la Computación, a pesar de que
la materia no se restringe a las computadoras y de que sus practicantes no usan
necesariamente el método científico. Incluso la Asociación de Maquinaria
Computacional (ACM)[^acm] intenta conseguir permiso para utilizar el nombre
pero la compañía rechaza la petición.

Cabe destacar que, según Donald Knuth[^donald-knuth], la elección del término
«*Computer Science*» por las universidades americanas no se debe a un problema
de marca comercial sino a razones semánticas: las computadoras tratan *datos*,
no *información*.

Por muy pragmática que pueda parecer, esta elección describe, de forma
deliberada, únicamente el *cómo* de la informática sin prestar atención al
*porqué*.

Por supuesto, es cierto que los ordenadores tratan datos, pero nuestro objetivo
al utilizarlos es tratar información.

[^karl-steinbuch]: **Dr. Karl W. Steinbuch** fue un científico informático
  alemán, cibernético, e ingeniero eléctrico. Es uno de los pioneros de la
  informática alemán, así como, con la Lernmatrix, uno de los pioneros de las
  redes neuronales artificiales. Steinbuch también escribió acerca de las
  implicaciones sociales de los medios de comunicación modernos. —*Karl
  Steinbuch^`W`^*

[^philipe-dreyfus]: **Philippe Dreyfus** es un pionero de la informática en
  Francia.  Después de obtener su Licenciatura en Física en 1950, fue profesor
  en la facultad de Informática en la Universidad Harvard usando Mark I, el
  primer ordenador automatizado jamás construido. En 1958 fue nombrado director
  del Centro de Cálculo Bol. —*Philippe Dreyfus^`W`^*

[^acm]: **ACM** acrónimo de *Association for Computing Machinery* (Asociación
  de Maquinaria Computacional). Fue fundada en 1947 como la primera sociedad
  científica y educativa para educar acerca de la Computación. Publica varias
  revistas y periódicos científicos relacionados con la computación, patrocina
  conferencias en varias áreas del campo y otros eventos relacionados con las
  ciencias de la computación como por ejemplo el internacional Competición
  Internacional Universitaria ACM de Programación (ICPC). Publica una extensiva
  biblioteca digital y una referencia de la literatura de la computación.
  —*Association for Computing Machinery^`W`^*

[^donald-knuth]: **Donald Ervin Knuth** es uno de los más reconocidos expertos
  en ciencias de la computación por su fructífera investigación dentro del
  análisis de algoritmos y compiladores. Es Profesor Emérito de la Universidad
  de Stanford. —*Donald Knuth^`W`^*



## Algunas definiciones {-}

A pesar de ser un campo erigido sobre valores binarios[^binary-values], la
informática está llena de dicotomías interesantes. La fundamental, que suele
ser ignorada, es la diferencia entre *datos* e *información*:

> **Información**, del Latín *informo*, «Yo construyo dentro (de mí mismo)»:  
> Una idea, un constructo de la mente humana que puede ser compartida con
> otros.

> **Dato**, del Latín *datum*, «lo que se da»:  
> Una de las posibles representaciones de una pieza de información que puede
> ser transferida e interpretada como información por humanos.

La información sólo existe en la mente humana.

No todas las construcciones de la mente humana son información, sólo lo son
aquellas que pueden ser transferidas a otro ser humano de forma precisa. Por
ejemplo, las experiencias místicas no pueden ser consideradas información
porque no pueden transmitirse.

Sin embargo, la información es la pieza fundamental del conocimiento humano. En
realidad, todo lo relacionado con el campo de las matemáticas[^mathematics] es
información, hasta el punto de que ninguna conjetura puede ser considerada
válida hasta que otras personas estén de acuerdo con ella tras leer la
descripción de la prueba formada en la mente del autor.

Los datos, en cambio, son meras representaciones.

Cualquier representación de información es un conjunto de datos.

Las palabras que estás leyendo, ya sean impresas en un papel o proyectadas en
una pantalla, representan el mensaje que trato de comunicar, una reflexión
construida tras años de experiencia en el sector.

Pero podría haber grabado un vídeo o un disco de vinilo.

Todos esos soportes hubiesen representado la misma información, pero no serían
equivalentes.

[^binary-values]: El sistema binario es un sistema de numeración en el que los
  números se representan utilizando solamente dos cifras: cero y uno (0 y 1).
  A pesar de que existen máquinas que utilizan otros sistemas, el sistema
  binario es, por su sencillez, el más usado en las máquinas de computación.

[^mathematics]: «**Matemáticas**, del griego Μαθημα-τικὴ, «el arte de
  aprender». La matemática estudia la estructura de los constructos de la mente
  humana (conceptos, patrones, percepciones...) que pueden ser comunicados con
  precisión a través de un lenguaje.  [...]

    La matemática sólo trata de nuestra mente.» — Giacomo Tesio  
    <http://www.tesio.it/2018/10/11/math-science-and-technology.html>

### Una relación complicada {-}

La relación entre información y datos es muy compleja.

Escribiendo estas palabras estoy convirtiendo la información de mi mente en
datos. Al leerlas, estás convirtiendo estos datos en información de nuevo.

Ambas frases están expresadas en presente, pero ocurren en diferentes puntos
del espacio y del tiempo.

Los datos suelen acarrear más información que el mensaje que se pretende
transmitir. Por ejemplo, este mismo texto aporta más información que su propio
contenido: el lenguaje utilizado, su longitud, etc. Esta información se suele
denominar, impropiamente, «*metadatos*[^metadata]». Pero incluso gran cantidad
de información puede embeberse dentro de estos metadatos como, por ejemplo, lo
que el estilo de mi escritura puede revelar sobre mí.

Si en lugar de un mensaje en forma de texto, esta información hubiese sido
compartida mediante un vídeo, la cantidad de información adicional compartida
hubiese sido mucho mayor. Mi raza, mi género, mi edad, incluso algunas posibles
enfermedades podrían haberse dejado entrever a través del soporte elegido. Y mi
voz y mucho, mucho, más.

Entonces, podemos ver que **la información puede convertirse en datos** y los
**datos pueden convertirse de nuevo en información** por la mente humana, pero
no existe ninguna función matemática que pueda describir esta relación: existe
una pérdida y una ganancia de información en cada paso.

Y las personas pueden malinterpretar los datos creando información
completamente diferente a la que quien creó el mensaje pretendía compartir.

Aun así, los humanos somos tan buenos convirtiendo información en datos y datos
en información que ni siquiera somos conscientes del proceso. Y esto nos ha
llevado a montones de desafortunados y dramáticos[^lethal] malentendidos en la
informática.

[^metadata]: Los **metadatos** (del griego μετα, meta, 'después de, más allá
  de' y latín datum, 'lo que se da', «dato»), literalmente «sobre datos», son
  datos que describen otros datos. —*Metadatos^`W`^*

[^lethal]: El 9 de abril de 2019 «CNN Business» publica que tras los accidentes
  sufridos por el «Boeing 737 Max» en Etiopía, donde todos los pasajeros y la
  tripulación murieron, las ventas de este modelo han caído en picado hasta
  rozar la desaparición.  
  <https://edition.cnn.com/2019/04/09/business/boeing-737-max-deliveries/index.html>

    El 18 de abril de 2019 el IEEE-Spectrum publica que todo apunta a que un
  fallo de software ha provocado los accidentes. Según el artículo, los cambios
  del 737 que llevaron a la creación del 737 Max supusieron alteraciones en el
  modo de vuelo de los aviones, que dejaron de volar con la naturalidad
  habitual. Para solventar estas diferencias, en lugar de editar el diseño
  físico del 737 Max, lo que acarreaba unos costes de producción muy altos,
  Boeing produjo su sistema «*Maneuvering Characteristics Augmentation System*»
  o MCAS. Este sistema *software* altera el pilotaje del 737 Max haciéndolo
  parecer un 737, corrigiendo la posición de la nariz del avión para suplir el
  desplazamiento provocado por tener unos motores de mayor tamaño.

    Este cambio de percepción supone una reducción en la capacidad para sentir
  la respuesta del avión en tiempo real. El sistema MCAS se basa además en
  tomar datos de sensores con una propensión a fallos importante e, incluso
  con el piloto automático apagado, no permite tener el control total del
  vuelo.

    El artículo propone que el MCAS, nacido como un sistema de seguridad para
  reducir costes, acaba matando a más gente (346 personas) que a la que habría
  salvado y añade que, en lugar de añadir más complejidad de software sobre
  éste, la solución parte de eliminarlo por completo.  
  [`https://spectrum.ieee.org/aerospace/aviation/how-the-boeing-737-max-disaster
  -looks-to-a-software-developer`][spectrum-ieee]


## Entonces, ¿qué es la informática? {-}

> **Informática**
> del francés *informatique*, «information automatique».
>
> Campo del conocimiento humano que estudia cómo la información puede ser
> transferida, almacenada, representada, interpretada y transformada, así como
> el conjunto de herramientas que se relacionan con dicho conocimiento.

La informática trata sobre información.

Trata sobre humanos y no sobre máquinas. Las computadoras son meros reflejos de
nuestra mente.

Como ocurre habitualmente, esta perspectiva europea es diametralmente opuesta a
la americana atribuida a Knuth.

Incluso en un conocido ensayo[^knuth-essay], Knuth define la *Ciencia de la
Computación* como el «estudio de los algoritmos» siendo estos tan ajenos a las
computadoras que incluso llega a usar el clásico juego de las sillas para
explicar el funcionamiento interno de una tabla hash[^hash-table] en ese mismo
artículo.

![Juego de las sillas usado por Knuth en «*Computer Science and its relation to
Mathematics*»][chairs]

[chairs]: img/chairs.png

El problema de la nomenclatura no es *sólo* filosófico: tras miles de años de
historia, sabemos que las palabras que usamos forjan nuestro entendimiento de
la realidad. Centrándonos en las computadoras y lo que somos capaces de hacer
con ellas, nos negamos la posibilidad de entender la amplitud del campo la
informática.

Los algoritmos son información en la mente de las personas que los conocen.

Si la informática sólo tratara de algoritmos, la vida de quien se dedica a la
programación sería mucho más fácil y aburrida. Desgraciadamente, **los
programas informáticos no son información sino datos**. Los programas no
existen en la mente humana, sino en un soporte físico **reproducible** por una
computadora, del mismo modo que un gramófono reproduce un disco de vinilo.

[^knuth-essay]: Ciencias de la computación y su relación con las matemáticas
  («Computer Science and Its Relation to Mathematics») (Abril de 1974).  
  [`http://www.maa.org/programs/maa-awards/writing-awards/computer-science-and- 
  its-relation-to-mathematics`][cs-and-math]

[^hash-table]: Una **tabla hash** [...] es una estructura de datos que asocia
  llaves o claves con valores. La operación principal que soporta de manera
  eficiente es la búsqueda: permite el acceso a los elementos (teléfono y
  dirección, por ejemplo) almacenados a partir de una clave generada (usando el
  nombre o número de cuenta, por ejemplo). Funciona transformando la clave
  [...] en un hash, un número que identifica la posición (casilla o cubeta)
  donde la tabla hash localiza el valor deseado. —*Tabla Hash^`W`^*


### Los errores pertenecen al mundo real {-}

Como datos que son[^as-data], los programas informáticos pueden representar
un algoritmo correcto de forma errónea por la mera razón de que *errar es
humano*[^errare-humanum-est].

Cada programa no es más que una de las posibles representaciones de un
algoritmo y, junto con el propio algoritmo, acarrea gran cantidad de
información añadida[^de-anonymizing-programmers].

Además, si quien programa no conoce con anterioridad el algoritmo **completo**
que pretende implementar, los errores que cometerá son otro ejemplo más del
concepto conocido como «Garbage In, Garbage Out[^garbage]».

Los errores, conocidos como *bug* (bicho en inglés), son tan inherentes a la
informática que los practicantes de la programación han creado un amplísimo
conjunto de herramientas y técnicas para cazarlos y
eliminarlos[^tools-to-hunt-and-kill]... sin demasiado éxito.

Pero este fracaso absoluto desde la perspectiva de la ingeniería puede suponer
una base sólida sobre la que construir la democracia.

[^as-data]: Muchos son los autores que afirman que los programas informáticos
  son datos y deben tratarse como tales. La familia de lenguajes LISP, se basa
  radicalmente en esta afirmación, mientras que otros lenguajes no la usan en
  absoluto. Desde un punto de vista técnico, los programas son datos por
  diversos motivos: ambos se almacenan del mismo modo y el código puede usarse
  como datos en muchas ocasiones (y viceversa). De hecho, los propios
  compiladores e intérpretes son programas que usan el código como datos,
  probando así que son perfectamente intercambiables en función de la
  situación. El artículo original enlaza a un texto que defiende esta
  afirmación con firmeza:  
  <http://wiki.c2.com/?DataAndCodeAreTheSameThing>

[^errare-humanum-est]: Referencia a «*Errare humanum est, sed perseverare
  diabolicum.*», frase atribuida a Séneca que significa literalmente: «Errar es
  humano, perseverar [en el error] diabólico».

[^de-anonymizing-programmers]: Como muestra, el siguiente artículo, que analiza
  métodos para de-anonimizar programas informáticos, esto es, conocer la
  autoría de los programas a través del estilo de programación. El artículo
  muestra que las personas más experimentadas, que están capacitadas para
  realizar tareas más complejas, son más fáciles de identificar que las novatas
  debido a que desarrollan un estilo personal que se ve reflejado en su código
  fuente.  
  Aylin Caliskan-Islam et al. *«De-anonymizing Programmers via Code
  Stylometry»*. Disponible online:  
  [`https://www.usenix.org/system/files/conference/usenixsecurity15/sec15-paper-  
  caliskan-islam.pdf`][caliskan-islam]

[^garbage]: En informática, el concepto **Garbage in, garbage out (GIGO)**
  describe que si se alimenta un proceso con datos absurdos o defectuosos los
  datos resultantes del proceso son también absurdos o «basura» (en inglés
  «garbage») —*Garbage in, garbage out^`W`^ (traducción propia)*

[^tools-to-hunt-and-kill]: Existen herramientas de verificación de software,
  como Adacore o TLA+, y técnicas, como la programación por pares y el
  desarrollo guiado por pruebas, cuya intención es prevenir la aparición de
  errores en el software. También se han desarrollado herramientas de
  corrección y análisis de éstos, conocidas como debuggers, como GDB (The GNU
  Debugger). A pesar de su existencia y de su amplio uso, el software sigue
  teniendo fallos. Además, la existencia de muchas herramientas para una misma
  labor suele ser una muestra de su falta de efectividad.

## Mucho más que computadoras {-}

Mientras que la matemática pertenece al dominio de la mente humana y es
comunicable (o no es matemática aún), cualquier concepto que pertenece a la
matemática es información. Como tal, también pertenece al campo de la
informática.

Lo contrario no es cierto: los errores (*bugs*) están muy presentes en la
informática pero no se dejan ver en el mundo de las matemáticas.

Como consecuencia, **las matemáticas son parte de la informática**.

Podría decirse que es al contrario o que ambas disciplinas son hermanas como la
física y la matemática. Pero una mirada más minuciosa nos muestra que para que
esta relación se cumpla es necesario retirar de la informática la criptografía,
la estadística y el diseño de interacción humana[^crypto-stats-ux].

Aunque esta afirmación puede parecer una herejía al principio, no debería
sorprendernos demasiado ya que podemos observar que la informática altera cada
aspecto de la vida humana, desde la medicina hasta la agricultura, desde la
reproducción a las finanzas, desde la ingeniería a la cocina, desde la
democracia hasta la guerra.

Y aunque ningún programa necesita medicina, muchos médicos usan programas.  
Y aunque ningún programa necesita matar, la mayoría de las guerras necesitan
software.

Y así, sucesivamente.

Este fenómeno tiene una explicación sencilla: la informática transforma todos
los aspectos de la vida humana porque **transforma la forma en la que la
humanidad piensa colectivamente**. La información pertenece al dominio de la
mente humana y, al compartirse con otros miembros de la comunidad, construye la
cultura de dicha comunidad. Y la cultura vuelve a la mente de los nuevos
miembros de la comunidad a modo de información en un bucle infinito.

La informática es hoy en día lo que la matemática en tiempo de
Pitágoras[^pythagoras]. Es filosofía disfrazada. Filosofía aplicada, si lo
prefieres.

[^crypto-stats-ux]: El autor enlaza varios conceptos o técnicas de cada uno de
  los campos que menciona con el objetivo de probar su afirmación.

    En referencia a la criptografía menciona el concepto «*Side-channel attack*»,
  ataque de canal lateral en español, cuyo fundamento es el de encontrar
  vulnerabilidades en la propia implementación física (electromagnetismo,
  consumo energético, acceso físico, acústico o visual al sistema,
  sincronización, etc.) de un sistema informático en lugar de tratar de atacar
  el algoritmo mismo.

    En referencia a la estadística, enlaza el concepto de «*Machine Learning*»,
  aprendizaje automático en español, que implica que una máquina sea capaz de
  aprender sin supervisión humana. Esto es, que, de forma autónoma, su
  desempeño en una tarea mejore con la experiencia. El campo del aprendizaje
  autónomo está relacionado de forma muy directa con la estadística
  computacional y con la optimización matemática.

    En referencia al diseño de interacción humana, el autor menciona el campo de
  la interacción persona-computadora, «*Human-computer interaction*» en inglés,
  donde se estudia la relación entre las personas usuarias y los sistemas
  informáticos. Este campo de estudio se sitúa en la intersección de la
  informática con las ciencias del comportamiento, el diseño y las ciencias de
  la información, entre otros.

[^pythagoras]: **Pitágoras de Samos** fue un filósofo y matemático griego del
  siglo V a.C. Famoso por el teorema que lleva su nombre.

## Un sector político {-}

Muchos practicantes no aceptarían la descripción de la informática como una
forma de filosofía moderna. Esto no se debe sólo a una falta de conocimiento
sobre la propia filosofía (donde nace la lógica), sino porque la filosofía
genera cierto rechazo. Después de todo, analizando la historia, desde Confucio
hasta Platón, desde Kant hasta Nietzsche, **los filósofos siempre han tenido el
engorroso hábito de meterse en política** de una forma u otra.

Esto es molesto. Los ingenieros de software intentan ser sólo ingenieros para
poder dedicarse **únicamente** a la parte técnica del trabajo. Se sienten
responsables de crear el artefacto que mejor cumpla el objetivo, pero no
quieren responsabilizarse de la elección del objetivo en sí mismo.

Ser filósofo implica posicionarse. Ser ingeniero, en cambio, implica
simplemente crear cosas de modo que puedes justificar no estar de acuerdo con
los valores de tu cliente mientras que sirves a sus propósitos.

Los ingenieros pueden fingir ser «neutrales», los filósofos no.

### La tecnología es la persecución de la política por otros medios {-}

Si miras a la historia, puedes ver cómo la tecnología es la mayor fuerza
política en el mundo.

Desde el fuego a la rueda, los barcos, los molinos: toda nueva tecnología crea
nuevos modos de vida que permiten la aparición de nuevas organizaciones
humanas.

Uno puede preguntarse: ¿cómo algo que trabaja con información puede alterar de
forma tan profunda el mundo físico en el que vivimos? ¡La información no es más
que un conjunto de pensamientos que podemos comunicar, al fin y al cabo!

Cuando se habla de que «el software se está adueñando del mundo», no se
plantea **cómo** está ocurriendo.

Resulta que la respuesta es una maravilla de la electrónica: la computadora
programable de uso general. Las computadoras de uso general no están diseñadas
para resolver un problema concreto sino para ejecutar una serie de
instrucciones entregadas por una persona en un formato binario.

De esta forma, cuando las computadoras reproducen el software, lo que ocurre es
que un acto de mera imaginación expresado en un lenguaje concreto es invocado
como una suerte de demonio[^daemon] que actúa en el mundo físico.

Como ha pasado anteriormente, cada avance en la tecnología aporta una
ventaja estratégica a quien tiene acceso al mismo. No necesitamos remontarnos a
la Edad de Piedra para encontrar evidencias de esto, ni siquiera necesitamos
conocer la máquina Enigma[^enigma]. Una Guerra Informática Mundial está
teniendo lugar ahora mismo. Una guerra por la total dominación del ser humano
mediante la tecnología.

[^daemon]: Juego de palabras intraducible. En sistemas POSIX se utiliza el
  término *«daemon»*, demonio en inglés, para referirse a programas que se
  ejecutan en segundo plano. En español el término no ha sido traducido y se
  usa el original en inglés.

[^enigma]: Enigma era el nombre de una máquina de cifrado mecánico creada y
  comercializada a principios del siglo `XX`. Durante la Segunda Guerra
  mundial, el ejército alemán utilizó versiones alteradas de estas máquinas. El
  ejército aliado, a pesar de las alteraciones, fue capaz de descifrar las
  comunicaciones en cantidad de ocasiones, siendo ésta una gran fuente de
  información para el ejército aliado durante la guerra.

### La programación entrena la racionalidad {-}

En sí misma, una computadora de uso general es inútil. Pero los programas la
pueden especializar para hacerla útil para una gran variedad de problemas
específicos.

Esta especialización **limita** lo que la computadora puede hacer. **Reduce**
su potencial. Esto puede parecer ilógico para el no versado en la programación
(o incluso para quien lo está), pero, en realidad, todo lo que hacemos con
lenguajes de programación es **reducir** lo que la máquina es **capaz de
hacer** decidiendo lo que **hará en realidad**.

¿Pero trata esto sólo sobre máquinas?

Si todo de lo que dispusiéramos fuera ensamblador[^assembly] podría ser así. La
programación no sería muy diferente al diseño de circuitos.

Sin embargo, se crearon lenguajes de programación de alto
nivel[^high-level-programming-languages] para acelerar el desarrollo de
software y, al hacerlo, se redujo el acoplamiento[^coupling] entre el
hardware y el software.

Los lenguajes de programación de alto nivel reducen la carga cognitiva de las
personas que programan. Gracias a ellos no necesitamos conocer los detalles
internos de un procesador o dispositivo (al menos, la mayor parte de las
veces[^meltdown]). Una vez liberadas de los límites de la máquina, las personas
se encuentran con otro límite: el de su propia mente.

Hay muchas formas de expresar un programa. La mayor parte incorrectas.

Por tanto, la práctica de la programación evolucionó hacia herramientas cada
vez más complejas constriñendo la forma en la que las personas pueden expresar
lo que tienen en mente, ayudándolas a escribir código que su mente pueda
manejar.

Sin embargo, como el hardware subyacente sigue estrictamente las normas de la
lógica y la matemática, cualquier lenguaje de programación debe hacerlas
cumplir también tarde o temprano.

Esto significa que **para programar** necesitas aprender **pensamiento
racional**. Y también necesitas explicarlo.

En otras palabras, programar fuerza a las personas a describir complejas
dinámicas y sistemas en constante evolución a alguien tan **tonto** como es un
ordenador. Algunas técnicas[^techniques-ii] son tan cercanas a la filosofía
que sus practicantes hablan más de lo que programan.

El lenguaje de programación que eliges influencia también tu forma de pensar en
un modo mucho más profundo que el que experimentas al aprender un lenguaje
humano. Los patrones de pensamiento que aprendes programando resultan útiles en
todos los aspectos de la vida. Es igual que lo que ocurre con la matemática,
pero aún más intenso.

Además, la forma clave de entender el potencial político de la informática es
la **depuración del código** (proceso conocido en inglés como «debugging»).
Durante el proceso de depuración **buscas un error en la elaboración cultural
colectiva de miles de personas** de todo el mundo.

[^assembly]: Se conoce como lenguaje ensamblador a cualquier lenguaje de
  programación cuya relación con el lenguaje de máquina sea muy cercana. Las
  órdenes de los lenguajes ensamblador se corresponden con una orden en
  lenguaje máquina.  Es por eso que no son órdenes similares a lo que un humano
  consideraría un proceso lógico, sino que están mucho más asociadas al
  funcionamiento del dispositivo físico subyacente. Las órdenes del lenguaje
  ensamblador indican las acciones que la máquina debe realizar, tales como
  mover un valor a un registro, saltar a una dirección de memoria concreta,
  etc. es por eso que el autor afirma que la programación en ensamblador no es
  muy diferente al diseño de circuitos.

[^high-level-programming-languages]: Los lenguajes de programación de alto
  nivel abstraen los conceptos subyacentes de modo que quien programa no debe
  preocuparse de las instrucciones de lenguaje máquina individuales sino que
  describe de forma más abstracta lo que desea conseguir y es la implementación
  del lenguaje la encargada de decidir qué órdenes de lenguaje máquina se
  ejecutan internamente.

[^coupling]: El acoplamiento (*coupling*) define el grado de interdependencia
  entre distintos elementos en un sistema. En este caso trata de la
  interdependencia entre el programa y la máquina en la que éste vaya a
  ejecutarse.

[^meltdown]: El autor muestra un caso en el que abstraerse del comportamiento
  de la máquina es peligroso. Las vulnerabilidades como Meltdown y Spectre son
  vulnerabilidades críticas que afectan a los procesadores debido a su manejo
  avanzado de las instrucciones. Ambas vulnerabilidades explotan la ejecución
  especulativa, una técnica de optimización aplicada por procesadores modernos.
  Más información en:  
  <https://meltdownattack.com/>

[^techniques-ii]: El texto original hace referencia al la página de Wikipedia
  del diseño guiado por el dominio (*Domain Driven Design* en inglés,
  normalmente abreviado como *DDD*) como un ejemplo de esto:

    «El **diseño guiado por el dominio**, en inglés: domain-driven design
  (DDD), es un enfoque para el desarrollo de software con necesidades complejas
  mediante una profunda conexión entre la implementación y los conceptos del
  modelo y núcleo del negocio.

    El DDD no es una tecnología ni una metodología, este provee una estructura
  de prácticas y terminologías para tomar decisiones de diseño que enfoquen y
  aceleren el manejo de dominios complejos en los proyectos de software.

    El término fue acuñado por Eric Evans en su libro "Domain-Driven Design -
  Tackling Complexity in the Heart of Software".» —*Diseño guiado por el
  dominio^`W`^*

### La depuración de código entrena el pensamiento crítico {-}

No existe nada en la tierra que pueda entrenar el pensamiento crítico tanto
como un par de décadas de depuración de software.

Ves a la computadora ejecutar billones de líneas de código y tienes que
adivinar dónde ocurre el error que genera el fallo en el comportamiento del
programa. Es una tarea tan difícil y tediosa que las personas y empresas
intentan evitarla por todos los medios, normalmente intentando evitar el
problema. Pero cuando **tienes que** arreglarlo, es una experiencia muy
**educativa**.

En primer lugar porque, la mayor parte de las veces, es culpa tuya.

Pero otras veces es el compilador, otras el sistema operativo, otras el
navegador web y otras veces ves el efecto mariposa[^butterfly-effect] ocurrir
frente a tus ojos y tú tienes que encontrar (y matar) la mariposa correcta a
kilómetros de distancia, sólo para parar el tornado en el que te encuentras.

Comparado a esto, **destapar *fake-news* (noticias falsas) es un juego de
niños**.

¡Comparado a esto, destapar las afirmaciones de los poderosos es un juego de
niños! 

Esto ocurre porque estás entrenado en pensar sobre lo que miles de personas
pensaron antes que tú, entender sus suposiciones, para vislumbrar no sólo lo
que sabían o malentendieron, sino también lo que no conocían en absoluto.

Lo desconocido es parte fundamental de la informática.

Quien de verdad se dedica a la programación sabe que no sabe
nada[^socratic-method] por la propia experiencia. Y los *hackers* de verdad
saben que **nadie sabe nada**. ¡Por eso somos tan curiosos!

[^butterfly-effect]: El efecto mariposa es un concepto de la teoría del caos
  que describe que en algunas situaciones una pequeña perturbación puede
  terminar amplificándose para acabar generando un efecto enorme más adelante.
  El efecto mariposa se describe en lenguaje común como que el aleteo de una
  mariposa puede generar un tornado a kilómetros de distancia. En el proceso de
  depuración de software es muy frecuente alterar pequeñas secciones del
  programa y ver como los efectos de estos cambios se ven amplificados y
  afectan a secciones inesperadas del programa.

[^socratic-method]: El autor hace referencia al método socrático aquí,
  enlazando su página de Wikipedia, la enciclopedia libre. La frase «Sólo sé
  que no sé nada» es un dicho derivado de los textos de Platón sobre Sócrates.
  Se relaciona también con el momento en el que la Pitia, el Oráculo de Delfos,
  afirmó que Sócrates era el hombre más sabio, siendo esta frase una de las
  respuestas posibles de Sócrates a tal afirmación.

## Un derecho humano universal {-}

En la declaración universal de los derechos humanos[^human-rights] se recogen
tres artículos que tratan accidentalmente sobre la informática.

> **Artículo 12**:
>
> Nadie será objeto de injerencias arbitrarias en su vida privada, su familia,
> su domicilio o su correspondencia, ni de ataques a su honra o a su
> reputación. Toda persona tiene derecho a la protección de la ley contra tales
> injerencias o ataques.
>
> **Artículo 19**:
>
> Todo individuo tiene derecho a la libertad de opinión y de expresión; este
> derecho incluye el no ser molestado a causa de sus opiniones, el de
> investigar y recibir informaciones y opiniones, y el de difundirlas, sin
> limitación de fronteras, por cualquier medio de expresión.
>
> **Artículo 27**:
>
> 1. Toda persona tiene derecho a tomar parte libremente en la vida cultural
>    de la comunidad, a gozar de las artes y a participar en el progreso
>    científico y en los beneficios que de él resulten.
>
> 2. Toda persona tiene derecho a la protección de los intereses morales y
>    materiales que le correspondan por razón de las producciones científicas,
>    literarias o artísticas de que sea autora.

Incluso sin considerar las grandes multinacionales que constituyen el núcleo
del capitalismo de la vigilancia[^surveillance-capitalism], podemos ver estos
artículos ser violados sistemáticamente en la mayor parte de los dispositivos
que «poseemos».

Quienes sean incapaces de configurar su propio servidor de correo electrónico
no pueden dar por cumplido el artículo 12. Quienes no sepan programar están
incapacitados para «difundir \[informaciones\], sin limitación de fronteras,
por cualquier medio de expresión» y por tanto no pueden dar por cumplido el
artículo 19. Quienes no sepan depurar una librería de criptografía no pueden
dar por cumplido el artículo 19. Como los programas son cultura (y teoremas,
ver la correspondencia de Curry y Howard[^curry-howard]), las personas
incapaces de programar no pueden dar por cumplido el 27.1.

Así que **saber informática es un derecho humano**.

[^human-rights]: La declaración completa puede leerse en la página oficial de
  las Naciones Unidas:  
  <https://www.un.org/es/universal-declaration-human-rights/index.html>

[^surveillance-capitalism]: El capitalismo de la vigilancia es un concepto
  popularizado por Shoshana Zuboff en su libro del mismo nombre y que se
  refiere a la mercantilización de los datos personales con fines lucrativos.

    El texto original enlaza, sin embargo, un artículo de Eugeny Morozov en The
  Baffler que trata sobre el libro de Zuboff y analiza la historia del
  término y el estado de la tecnología a día de hoy y su reciente evolución.
  El artículo critica a Zuboff argumentando que muestra mucho más interés en
  criticar la vigilancia que el sistema capitalista que, en su opinión, es el
  que la provoca. El artículo original puede leerse en el siguiente enlace
  (en inglés):  
  <https://thebaffler.com/latest/capitalisms-new-clothes-morozov>

    El periodista Ekaitz Cancela tradujo el artículo al español para el diario
  El Salto en dos partes:  
  [`https://www.elsaltodiario.com/tecnologia/los-nuevos-ropajes-del-capitalismo-  
  parte-i`][salto-1]  
  [`https://www.elsaltodiario.com/tecnologia/evgeny-morozov-nuevos-ropajes-  
  capitalismo-zuboff-surveillance-capitalism-ii`][salto-2]

[^curry-howard]: En teoría de la demostración y teoría de lenguajes de
  programación, **la correspondencia de Curry-Howard** (también llamada
  isomorfismo de Curry-Howard) es la relación directa que guardan las
  demostraciones matemáticas con los programas de ordenador. Es una
  generalización de una analogía sintáctica entre varios sistemas de la lógica
  formal y varios cálculos computacionales que fue descubierta por primera vez
  por Haskell Curry y William Alvin Howard.  —*Correspondencia de
  Curry-Howard^`W`^*


## Un derecho fundamental {-}

¿Quieres vivir en un mundo donde necesites un escriba para leer y enviar **tu**
correo?

Sí, asumamos que son profesionales bien entrenados. Asumamos que tienen un
Código de Conducta, un juramento y todo eso.

Asumamos que es un servicio **gratuito**, como Gmail, Facebook o WeChat, y que
todos los escribas tienen trabajos bien pagados.

¿Qué puede ir mal?


### Libertad como en el software libre {-}

Vivimos en una distopía que hemos sido entrenados para ignorar.

Nunca deberíamos aceptar que un extraño leyera o escribiera nuestro correo. Ni
siquiera gratuitamente.

Pero aceptamos que, sin ninguna supervisión, un software escrito por extraños
controle nuestros dispositivos. Que actúe por nosotros.

Pueden leer lo que escribimos. Escuchar lo que decimos. Ver lo que hacemos.
Deciden lo que debemos o no debemos saber.

A principios de los años ochenta[^talking-to-the-mailman] del pasado siglo,
Richard Stallman de algún modo previó lo que ocurriría y fundó el movimiento
político[^political-movement] del software libre para combatirlo. Concibió las
cuatro libertades del software libre[^four-freedoms] para: usar, **estudiar**,
compartir y **mejorar** el software que recibes.

Más tarde, la iniciativa *open source* convirtió esos valores en herramientas
de marketing[^open-source].

Vaciadas de sus valores éticos[^emptied-of-values], las cuatro libertades se
convirtieron en una herramienta para ganar participación en el mercado y valor
para los accionistas.

Google fue probablemente el primero en darse cuenta de que es posible publicar
software que cumple **formalmente** las cuatro libertades mientras se mantiene
total control sobre el mismo. El truco es aumentar **la complejidad técnica**
hasta tal punto que nadie es capaz de poner en cuestión tu control del
proyecto.

De esta forma, haces como que cumples los valores *hackers* mientras que los
**marginas**.  Mediante esos valores consigues que los usuarios confíen en ti.
Usuarios que usan tu software **gratis** pero a cambio de **su propia**
**libertad**[^mass-surveillance] y **seguridad**[^safety].

Hoy en día hasta Microsoft[^linux-is-cancer] distribuye
GNU/Linux[^gnu-linux-in-windows].

¿Gano Richard Stalman[^open-source-2]?


[^talking-to-the-mailman]: El texto original enlaza aquí una entrevista (en
  inglés) realizada a Richard Stallman conocida como «Talking to the mailman»
  (hablándole al cartero). En ella, Richard Stallman habla de los orígenes del
  software libre.  
  [`https://newleftreview.org/issues/II113/articles/richard-stallman-talking-to
  -the-mailman`][mailman]

[^political-movement]: El texto original hace referencia al anuncio inicial del
  proyecto GNU, que nace con la intención de crear un sistema operativo libre.
  El texto, a pesar de no posicionarse políticamente de forma explícita es, a
  juicio del autor, justificación suficiente para su afirmación de que el
  software libre es un movimiento político. El enlace a continuación es
  una traducción oficial que incluye además el texto original en inglés:  
  <https://www.gnu.org/gnu/initial-announcement.es.html>

[^four-freedoms]: El movimiento del software libre define las cuatro reglas que
  el software debe cumplir para considerarse libre. Estas reglas se conocen
  como las cuatro libertades, y son las siguientes:

    - El software debe poder usarse para cualquier fin sin restricciones
      de ningún tipo.

    - El software debe poder ser estudiado sin ninguna restricción.

    - El software debe poder compartirse libremente.

    - El software debe poder modificarse por cualquiera y sus versiones
      modificadas deben poder compartirse sin restricciones.

[^open-source]: El autor enlaza un artículo de Evgeny Morozov (en inglés) para
  The Baffler donde el intelectual bielorruso critica a Tim O'Reilly y su
  capacidad para explotar nuevos términos como mecanismo de marketing.  El
  éxito más importante de Tim O'Reilly es el de la mercantilización del término
  «open source» (código abierto) en contra de la idea de software libre
  propuesta por Richard Stallman, aunque también se le atribuye la creación de
  otros términos como la Web 2.0.

    Según el artículo, O'Reilly, con la ayuda de Eric S. Raymond, popularizó a
  finales de los años noventa el concepto del código abierto con la única
  intención de hacerlo amigable desde un punto de vista corporativo, dejando de
  lado los valores que definen al software libre. A partir de ese momento
  O'Reilly se ha esforzado en hacer que la idea del código abierto forme parte
  del imaginario colectivo, declarando en muchas ocasiones que software
  anterior a la propia existencia del término fue desarrollado como «open
  source», negando la existencia del software libre o de unos valores
  anteriores a su movimiento franquicia. Este método, que también aplica
  continuamente a otros de sus términos favoritos, como «transparencia» o
  «participación», tiene como intención cambiar el pasado controlando la
  terminología técnica que cumple con sus intereses ideológicos y económicos.

    El artículo describe a Tim O'Reilly como una persona muy influyente, que
  gracias a su gigantesca editorial de libros tecnológicos, valorada en cientos
  de millones de dólares estadounidenses, dispone de todo el mecanismo de
  publicación necesario para imponer su discurso tecnológico a la gente de a
  pie, mientras que su don de la palabra y su posición de pensador en el sector
  de la tecnología en Silicon Valley le permiten distribuir su mensaje en las
  esferas más elevadas, como los gobiernos y la industria del software en
  general.  
  <https://thebaffler.com/salvos/the-meme-hustler>

[^emptied-of-values]: El proyecto GNU, y la comunidad del software libre en
  general, siempre han considerado que el *open source* o código abierto pierde
  de vista lo esencial al defender las ventajas prácticas (acceso al código
  fuente) frente a unos valores éticos (las libertades de las personas).
  Richard Stallman escribió una larga explicación de esto titulada «El código
  abierto no llega al centro del problema», que puede leerse en el siguiente
  enlace (en español):  
  <https://www.gnu.org/philosophy/open-source-misses-the-point.es.html>

[^mass-surveillance]: El autor enlaza aquí la página de Wikipedia *«Mass
  Surveillance Industry»* (sólo disponible en inglés), la industria de la
  vigilancia masiva, que muestra cómo el negocio de la vigilancia masiva surge
  como sector en 2001 hasta llegar a ser una industria multimillonaria en 2011
  usando como excusa la lucha antiterrorista. El enlace detalla además cómo los
  diferentes agentes gubernamentales utilizan empresas para vulnerar los
  derechos de humanos a través del uso retorcido de leyes de copyright y
  técnicas de engaño para obtener un consentimiento, generalmente no informado,
  de las personas usuarias de plataformas privadas.  
  <https://en.wikipedia.org/wiki/Mass_surveillance_industry>

[^safety]: En el texto original, la palabra «seguridad» enlaza a una lucha
  personal del autor en la que pretendía concienciar sobre los fallos de
  seguridad que los navegadores web modernos sufren debido a su propio diseño.
  El enlace es un reporte de un fallo en el sistema de gestión de incidencias
  de Mozilla Firefox (en inglés) en el que el autor describe diferentes fallos
  de seguridad y que recibe como respuesta que ése es el funcionamiento de la
  web y llegan incluso a preguntarle si se trata de una broma. El reporte de
  fallo está cerrado actualmente, ya que no existe ninguna intención de
  mitigarlo o resolverlo, pero las brechas de seguridad asociadas siguen
  abiertas. En los mensajes del autor puede encontrarse información más
  avanzada sobre qué tipo de ataques pueden realizarse.  
  <https://bugzilla.mozilla.org/show_bug.cgi?id=1487081#c16>

[^gnu-linux-in-windows]: Desde el año 2016 es posible instalar un subsistema
  GNU/Linux en Windows. El texto original enlaza a la documentación de Windows
  sobre cómo realizar la instalación.

[^open-source-2]: Al hilo de un enlace anterior sobre la diferencia entre
  código abierto y software libre, el autor recupera la misma discusión
  propuesta en el artículo de Richard Stallman en el que cuenta que el Código
  Abierto se centra en la parte técnica en lugar de la parte ética. En esta
  ocasión, enlaza a un texto titulado *«Open source still misses the point»*
  (en inglés), que se traduce como «El Código Abierto sigue sin llegar al
  centro del problema», haciendo referencia al título del artículo enlazado
  previamente:  
  <https://abhas.io/open-source-still-misses-the-point/>

[^linux-is-cancer]: Según reporta The Register en el enlace añadido por el
  autor (en inglés), en 2001, el entonces CEO de Microsoft, Steve Ballmer,
  declaró para Chicago-Sun Times que «Linux es un cáncer» y «contamina todo el
  software con su basura hippie GPL».  
  <https://www.theregister.co.uk/2001/06/02/ballmer_linux_is_a_cancer/>

### Más allá de la libertad para unos pocos {-}

¿Podemos llamar «libertad» a un derecho que pocos pueden practicar? ¿No
deberíamos, en su lugar, llamarlo «privilegio»?

Como la mayor parte de las personas no es capaz de programar y depurar
programas son incapaces de leer y modificar software libre. No pueden practicar
dos de las Cuatro Libertades. Están forzadas a **confiar** en otras. Y no
tienen forma de saber si los demás son confiables.

La disponibilidad del código fuente hace que el software libre sea
**teóricamente** más seguro que el software privativo, pero su complejidad
puede contrarrestar esto en gran medida. Una pieza de código maliciosa puede
mantenerse oculta durante meses[^malicious-piece] a pesar de la retórica open
source[^os-rethoric] del número de ojos.

Los usuarios **están obligados** a confiar en el sistema. No tienen opción. No
tienen libertad.

Entonces tenemos que ir más allá del software libre. Tenemos que convertir sus
libertades en derechos **universales**.

[^malicious-piece]: El 23 de marzo de 2016 The Register publicó (en inglés):
  «Cómo un programador ha roto Node.js, Babel y miles de proyectos con
  al retirar de NPM 11 líneas de JavaScript que todo el mundo estaba usando»  
  <https://www.theregister.co.uk/2016/03/23/npm_left_pad_chaos/>

[^os-rethoric]: El autor enlaza aquí al capítulo quinto del texto «La catedral
  y el bazar» escrito por Eric S. Raymond, impulsor del concepto *open source*,
  a finales de los años 90. Pueden encontrarse traducciones del texto a otros
  idiomas con relativa facilidad.  
  Éste es el texto original, publicado en la web de Eric S. Raymond:  
  <http://catb.org/~esr/writings/cathedral-bazaar/cathedral-bazaar/ar01s05.html>


## La tecnología de la información debe progresar {-}

Para convertir la informática de una herramienta de poder a una herramienta de
libertad tenemos que mejorarla drásticamente.

Tal y como los escribas hicieron con los jeroglíficos en el antiguo Egipto,
quienes programan obtienen su poder usando herramientas **primitivas** que
lleva años aprender a manejar.

E, igual que los escribas, no son conscientes de su propio poder y siguen
sirviendo al faraón[^capitalism] que los oprime.

Al entender que la informática trata sobre información y la información reside
dentro de sus propias cabezas, quienes se dedican a programar se darán cuenta
de que son *únicos*[^unicum] en la historia de la economía.

[^unicum]: En la versión original, debido a la influencia del italiano, el
  autor usa el término latino «unicum» que, a pesar de que la palabra latina se
  traduce como «único» al español, en italiano este término hace referencia a
  piezas únicas de colecciones o eventos históricos que son irrepetibles.

**Los programadores son los primeros trabajadores que controlan los medios de
producción**. Los tienen sólidamente unidos a su cuello y no pueden
eliminárselos sin destruir el Capital.

Si tu trabajo es programar, piensa en ello.  
No es tu IDE el que escribe el programa. No es tu escritorio. No es tu jefe.
Eso sólo son herramientas que «facilitan» lo que haces. Son útiles, pero
secundarias.  
Aun así: ¿quién decide lo que haces?

Ahora piensa lo que podrías hacer por este mundo en lugar de maximizar el valor
accionarial[^shareholders-value].

[^capitalism]: En el texto original en la palabra «faraón» se encuentra, como
  en otros casos, un enlace a Wikipedia, la enciclopedia libre. En este
  caso, para la sorpresa del lector, el enlace no es a la página «faraón» sino
  a «capitalismo».

[^shareholders-value]: Esta frase enlaza en el texto original a una charla del
  CEO de la empresa Purism, Todd Weaver, que desarrolla dispositivos
  electrónicos centrados en la privacidad y seguridad de quien los usa. En la
  charla, Weaver desarrolla su argumentación partiendo de que las empresas
  modernas tienen como único fin aumentar el valor sus acciones.  
  <https://puri.sm/posts/the-future-of-computing-and-why-you-should-care/>


### Evita el moralismo {-}

Desde hace años[^years-moralism], ha habido mucho ruido acerca ~~del
moralismo~~ la ética en la tecnología de la información.

En el campo de la inteligencia artificial[^ia-delusion], tras varias muertes
causadas porque los coches autónomos[^self-driving-cars] son incapaces de
resolver un «dilema del tranvía»[^trolley-problem] que no debería estar
ahí[^shouldnt-be-there], los investigadores tratan de enseñar ética a las
máquinas[^machine-morality].

Es muy inteligente, si lo piensas.  
Desde la perspectiva práctica es como enseñar sexo a los condones. Deberías
empezar por las personas, en su lugar.  
Pero desde el punto de vista político es un intento sutil de reducir la
responsabilidad corporativa[^corporate-acountability] del daño causado por sus
productos «autónomos».

No es ética, es moralismo: una perversión de la moralidad para servir los
intereses individuales.

Debemos rechazar esta hipocresía como la cortina de humo que es.

[^years-moralism]: En este punto el autor enlaza un artículo de The
  Conversation del año 2015 (en inglés) donde, tras el engaño de Volkswagen en
  las pruebas de contaminantes en los Estados Unidos de América, en las que los
  coches, mediante un mecanismo software, reducían su potencia para aparentar
  ser menos contaminantes de lo que eran realmente, se plantea una discusión de
  ética en la informática. El artículo recoge las opiniones de dos expertos del
  entorno de la informática en Australia que hablan de la importancia de un
  código ético en el sector. El primero de ellos menciona un punto interesante
  diciendo que la mayor parte de las personas que se dedican a la informática
  trabajan en el sector privado y muchas empresas tratan de conseguir beneficio
  por encima de cualquier cosa, dejando en segundo plano tanto la ley como la
  ética, y presionan a sus trabajadores para ser cómplices de ello.  
  [`https://theconversation.com/a-code-of-ethics-in-it-just-lip-service-or-
  something-with-bite-32807`][conversation]

[^ia-delusion]: En este punto el autor enlaza los contenidos de una de sus
  charlas (en inglés) llamada «El engaño de las redes neuronales», una crítica
  a la mal llamada inteligencia artificial, donde el autor analiza su
  comportamiento a nivel técnico y plantea varios problemas éticos como el mal
  uso de la inteligencia artificial y el uso de una terminología que nos aleja
  de la realidad técnica de ésta con el fin de humanizarla y que parezca que
  las computadoras son capaces de pensar, cuando en realidad no lo son.  
  <http://www.tesio.it/documents/2018_Milano_The-Delusions-of-Neural-Networks.pdf>

[^self-driving-cars]: El autor añade aquí un enlace a una traducción y
  adaptación del contenido de una de sus charlas en la que propone que la
  relación humano-máquina que es parte de nuestra vida actual es una simbiosis,
  pero que para serlo correctamente ambas partes deben coevolucionar. Según el
  autor, para que las personas evolucionen necesitan educación y conocer en
  profundidad el otro lado de la relación: la informática.  
  <http://www.tesio.it/2018/10/06/the-intelligent-symbiosis.html>

[^trolley-problem]: El dilema del tranvía es un dilema ético cuyo enunciado
  dice lo siguiente:  
  «Un tranvía corre fuera de control por una vía. En su camino se hallan
  cinco personas atadas a la vía. Es posible accionar un botón que encaminará
  al tranvía por una vía diferente, pero hay otra persona atada a ésta.
  ¿Debería pulsarse el botón?»

    El dilema ha sido estudiado en profundidad, cambiando la cantidad de
  personas, sus ocupaciones, etc. El texto original enlaza aquí la Máquina
  Moral del MIT (en múltiples idiomas, que incluyen el español), un proyecto
  que pretende recoger la opinión del público sobre las decisiones que los
  vehículos autónomos deben tomar. Todas ellas son extensiones del problema del
  tranvía, pero se proponen desde una asunción peligrosa: la de que es
  necesario tomar la decisión de quién vive y quién muere.  
  <http://moralmachine.mit.edu/>

[^shouldnt-be-there]: Aquí el autor enlaza una discusión (en inglés) que ha
  mantenido con el proyecto «*Awful AI*» («IA Horrible» en español), que tiene
  como objetivo listar usos inadecuados de la inteligencia artificial. En la
  discusión el autor propone el caso de los vehículos autónomos como un uso
  inadecuado de la inteligencia artificial y cita a la profesora de filosofía
  política Maria Chiara Pievatolo diciendo que: «en un sistema moral que no es
  utilitario, sino deontológico y basado en la dignidad de las personas, un
  sistema autónomo sólo puede ser aceptable si ha sido diseñado para evitar y
  reducir los riesgos de las personas, es decir, para evitar que el problema
  del tranvía pueda llegar a ocurrir.»  
  A fecha de edición la discusión sigue abierta:  
  <https://github.com/daviddao/awful-ai/issues/14>

[^machine-morality]: La **ética de las máquinas** (o moral de las máquinas) es
  la parte de la ética de la inteligencia artificial que trata el
  comportamiento moral de los seres con inteligencia artificial. La ética de
  las máquinas contrasta con la roboética, que se enfoca al comportamiento
  moral de los humanos mientras diseñan, construyen, usan y tratan a los seres
  con inteligencia artificial. La ética de las máquinas no debe ser confundida
  con la ética computacional, la cual se enfoca en el comportamiento
  profesional en el uso de las computadoras y la información.  —*Ética de las
  máquinas^`W`^*

[^corporate-acountability]: El 18 de Marzo de 2018 una mujer llamada Elaine
  Herzberg fue atropellada por un coche de pruebas de la empresa Uber
  funcionando en modo autónomo mientras un piloto humano de respaldo viajaba en
  el asiento del conductor. Elaine Herzberg fue el primer peatón en morir
  a manos de un vehículo autónomo. Las investigaciones llegaron a múltiples y
  contradictorias conclusiones: fallos de software, fallos en el reconocimiento
  del peatón y un informe policial preliminar que indicaba que era imposible
  evitar el accidente porque la víctima cruzó una carretera de forma insegura,
  teniendo lugares habilitados para hacerlo de forma segura y correcta.

### Sé humano {-}

Quien programa suele mirar a los usuarios desde la limitada perspectiva de la
aplicación que está desarrollando.

Quien programa decide con gran grado de libertad lo que los usuarios de la
aplicación resultante pueden o no pueden hacer. De esta forma, decide lo que
los usuarios deben entender y lo que no. Lo que probablemente pensarán y lo que
no.

Esto va más allá que los problemas de la vigilancia y es la base de la
interacción humano-máquina[^human-machine-interaction] y es... **inevitable**.

Pero en lugar de pensar en el usuario desde la limitada perspectiva del ámbito
de nuestra aplicación, debemos considerarlos seres humanos, debemos
considerarlos personas.

Si piensas en el software como en una carta que le envías a otra persona en
lugar de en una herramienta para ganar dinero, empiezas a fijarte en sus
problemas de otra forma.

¿Y si las personas al otro lado están cansadas?  
¿Y si necesitan resolver problemas inesperados con nuestro programa?  
¿Realmente entienden lo que hacen con tu software?  
¿Realmente entienden los riesgos de seguridad a los que se exponen?  
¿Les da miedo el software?  
¿Pueden configurarlo para cumplir sus necesidades?

**Llamar a la gente «usuario» es una forma de usarlos sin remordimiento**.
No hagas eso.

[^human-machine-interaction]: La **interacción persona-computadora o
  interacción persona-ordenador (IPO)** se podría definir formalmente como: «La
  disciplina dedicada a diseñar, evaluar e implementar sistemas informáticos
  interactivos para el uso humano, y a estudiar los fenómenos relacionados más
  significativos» —*Interacción persona-computadora^`W`^*


### Sé político {-}

Siendo la tecnología una persecución de la política por medios que **tenemos
bajo control**, debemos considerar qué responsabilidad viene con ellos.

Como personas liberadas debemos manifestar qué objetivos políticos
queremos conseguir con cada programa que hacemos.

Desde ahora, todos mis programas de software libre contendrán un nuevo archivo,
junto al `LICENSE.txt` y al `README.txt`: **`POLITICS.txt`**

**`POLITICS.txt`** debe ser un manifiesto **corto** pero **inequívoco** sobre
los efectos políticos que el autor del proyecto pretende conseguir con su
software.

Esa descripción **no** incluye una bonita lista de principios
éticos[^google-principles] que los autores quieren seguir en el fondo de su
corazón simplemente para evitar el riesgo de caer en la hipocresía.

Debe ser una lista de **efectos sociales** específicos que los autores quieren
producir **en la sociedad** con **este software en particular**. Y debe listar
también los efectos sociales que pretende evitar o minimizar.

Pueden ser de alto nivel o muy específicos, pero la gente debe ser capaz de
decir si el software es un **fracaso político o un éxito** sólo leyendo esos
objetivos y comparándolos con el efecto del software producido.

El éxito político puede ser un fracaso tecnológico y viceversa.  
Y el documento `POLITICS.txt` puede evolucionar con el proyecto.  
Y los proyectos pueden bifurcarse (*fork*) por desacuerdos sobre éste.

Y no pasa nada.

Pero el hecho de **tener un `POLITICS.txt` significa que te haces responsable**
de los demonios que invocas.

[^google-principles]: El autor se burla de la reciente moda de que las
  grandes corporaciones tengan una carta de principios éticos que sólo se
  utilizan como herramienta de marketing. En este caso enlaza a la de Google:  
  <https://ai.google/principles>


### Sé educativo {-}

La ética *hacker* que subyace al software libre está erigida sobre la
curiosidad. Todo trata sobre el deseo de aprender, explorar nuevas soluciones
y poner a prueba suposiciones generalmente aceptadas por válidas.

Pero nuestro tiempo es limitado y también lo es nuestra mente.

Por lo que una estrategia muy efectiva para conseguir nuevo conocimiento es
maximizar el número de personas buscándolo.

Escribir buen código no es tan efectivo como escribir código
simple[^simple-code].

Leer el código fuente no debería de requerir una licenciatura o años de
experiencia en el sector.

Es necesario que reorientemos la investigación en los lenguajes de programación
hacia la **simpleza**. Las lecciones del profesor Niklaus Wirth[^wirth] aún
están esperando una nueva vida[^wirth-lessons].

Necesitamos herramientas simples que encajen fácilmente en lugar de
herramientas complejas que puedan hacerlo todo.

No será fácil, ya que **la informática es aún un campo muy primitivo**. Igual
que los antiguos egipcios vivieron sin número cero durante cientos de años, aún
nos faltan muchos conceptos fundamentales. Pero tenemos que descubrirlos.
Tenemos que crear un alfabeto de la informática.

Con este alfabeto debemos liberar a la siguiente generación de esclavos.

[^simple-code]: Referencia a un proyecto que él mismo mantiene llamado Jehanne,
  un sistema operativo *simple* y a un texto que describe el funcionamiento de
  Jehanne para demostrar su simpleza (en inglés):  
  <http://jehanne.io/2018/11/15/simplicity-awakes.html>

[^wirth]: El profesor **Niklaus Emil Wirth** es un informático nacido en
  Winterthur, Suiza, en 1934, que diseñó muchos lenguajes de programación,
  entre ellos Pascal. En 1984 obtuvo el premio Turing, premio otorgado por la
  ACM que se considera el premio más prestigioso de la informática. Se puede
  saber más sobre él y acceder libremente a sus publicaciones en su página
  web:  
  <https://inf.ethz.ch/personal/wirth/>

[^wirth-lessons]: El profesor Wirth aún tiene disponibles unas lecciones sobre
  el lenguaje de programación Oberon, en cuyo diseño participó activamente, así
  como en el de sus predecesores Modula y Pascal. Las lecciones pueden
  visitarse en el siguiente enlace:  
  <https://miasap.se/obnc/oberon-report.html>

    Oberon es algo más que un lenguaje de programación, es el diseño
  completo de una computadora de escritorio pensado para que cualquier
  persona pueda implementar el sistema completo y mantenerlo en un entorno de
  producción. En su página web puede encontrarse más información:  
  <http://www.projectoberon.com/>


## La humanidad debe evolucionar {-}

Esto no trata sólo de quienes se dedican a desarrollar software.

Un médico no puede curar a un enfermo que se niega a ser tratado.

Si queremos preservar la democracia **tenemos** que hacerla evolucionar. No es
la democracia la que debe ser automatizada[^automated-democracy], la gente
debe ser educada.

Las personas deben darse cuenta de que son marionetas en las manos de grupos de
poder que deciden qué deben saber y pensar.

Algunos rechazarán ganar esa conciencia ya que muchas veces los oprimidos
interiorizan la opresión como parte de su propia
identidad[^oppression-as-identity]. Pero si les damos la oportunidad, puede que
permitan a sus hijos descubrir su camino hacia la libertad.  
Otros verán la realidad tal y como es, y lucharán... Estudiando.

Independientemente de lo que hagamos, la informática va a ser una
revolución[^revolution] de todas formas.

[^automated-democracy]: El texto original enlaza un artículo de Motherboard (en
  inglés), la sección tecnológica de Vice, que anuncia que DARPA, la agencia de
  proyectos de investigación avanzados de defensa del departamento de defensa
  estadounidense (*Defense Advanced Research Projects Agency*), está
  desarrollando un nuevo sistema de voto electrónico debido a las críticas
  recibidas por los sistemas de voto electrónico anteriores. El artículo
  manifiesta ciertas dudas sobre su posible solución, y detalla que el desafío
  es extremadamente complejo incluso para un proyecto de las dimensiones de
  éste (10 millones de dólares estadounidenses).  
  [`https://motherboard.vice.com/en_us/article/yw84q7/darpa-is-building-a-dollar10-
  million-open-source-secure-voting-system`][darpa-10mil]

[^oppression-as-identity]: «Pedagogía del Oprimido», Paulo Freire, 1970. 

[^revolution]: Una **revolución** (del latín *revolutio*, "una vuelta") es un
  cambio social fundamental en la estructura de poder o la organización que
  toma lugar en un periodo relativamente corto o largo dependiendo la
  estructura de la misma —*Revolución^`W`^*


> **Morfeo**: Matrix nos rodea, está por todas partes. Incluso ahora en esta
> misma habitación. Puedes verla si miras por la ventana, o al encender la
> televisión. Puedes sentirla cuando vas a trabajar, cuando vas a la iglesia,
> cuando pagas tus impuestos. Es el mundo que ha sido puesto ante tus ojos para
> ocultarte la verdad.
> 
> **Neo**: ¿Qué verdad?
>
> **Morfeo**: Que eres un esclavo, Neo. Igual que los demás naciste en
> cautiverio. Naciste en una prisión que no puedes ni saborear, ni oler, ni
> tocar. Una prisión para tu mente.

**La informática** es la pastilla roja[^matrix].

[^matrix]: Referencia a la película «Matrix», dirigida por las hermanas
  Wachowski, en 1999. Esta conversación Morfeo ofrece a Neo dos pastillas, con
  la azul vuelve a su vida normal y se olvida de lo que sabe sobre Matrix, con
  la roja sigue en su búsqueda de la verdad y la libertad.



[spectrum-ieee]: https://spectrum.ieee.org/aerospace/aviation/how-the-boeing-737-max-disaster-looks-to-a-software-developer
[darpa-10mil]: https://motherboard.vice.com/en_us/article/yw84q7/darpa-is-building-a-dollar10-million-open-source-secure-voting-system
[cs-and-math]: http://www.maa.org/programs/maa-awards/writing-awards/computer-science-and-its-relation-to-mathematics
[caliskan-islam]: https://www.usenix.org/system/files/conference/usenixsecurity15/sec15-paper-caliskan-islam.pdf
[salto-1]: https://www.elsaltodiario.com/tecnologia/los-nuevos-ropajes-del-capitalismo-parte-i
[salto-2]: https://www.elsaltodiario.com/tecnologia/evgeny-morozov-nuevos-ropajes-capitalismo-zuboff-surveillance-capitalism-ii
[mailman]: https://newleftreview.org/issues/II113/articles/richard-stallman-talking-to-the-mailman
[conversation]: https://theconversation.com/a-code-of-ethics-in-it-just-lip-service-or-something-with-bite-32807
