# Prefacio {-}

Inicialmente, cuando se concibe la idea de publicar estos tomos, no se plantea
éste como el primero de ellos. El plan, si es que había uno, no contemplaba la
opción de crear un tomo introductorio como éste que tienes entre manos, aunque
es cierto que esto daba sensación de empezar de una forma demasiado abrupta y
podía dar lugar a que se malinterpretara el objetivo de estas publicaciones.

Con el tema de aquel supuesto primer ejemplar ya seleccionado, en un intento de
reafirmarme en mis objetivos y comprobar si merece la pena el esfuerzo que esta
colección va a suponer, contacto con Giacomo y le cuento lo que pretendo hacer.

Y no me queda más remedio que comenzar por aquí. Y nada puede ser más perfecto.

En primer lugar porque, siendo ésta una colección de escritos técnicos cuyo
contenido está principalmente escrito por mí, no se me ocurre una mejor manera
de comenzar que con la opinión de otra persona y hacer el ejercicio de humildad
que requiere quedarme en un segundo plano en este tomo de apertura, tomo que
quizás sea el más importante en cualquier colección.

En segundo lugar porque la opinión de Giacomo y su forma de entender la
informática son, casualmente o no tan casualmente, las razones por las que este
ejercicio está teniendo lugar. Además, considero necesario hacértela llegar,
independientemente de si la comparto en su totalidad, porque creo que pone
sobre la mesa temas que como técnicos evitamos y debemos afrontar. Esa es,
pues, la idea detrás de *ElenQ Technology*.

—*Ekaitz Zarraga*


# Autoría y copyright {-}

## Autoría {-}

El texto original «¿Qué es la informática?» (o «*What's informatics?*» en
inglés) ha sido escrito por Giacomo Tesio y publicado en su blog[^blog].

[^blog]: [`http://tesio.it`](http://tesio.it)

La traducción del texto al español, su adaptación (introducción, notas al pie,
notas previas, etc.) y la publicación en este formato han sido realizadas por
Ekaitz Zárraga con el permiso del autor original y el correspondiente crédito
(ver párrafo anterior).

El contenido de muchas de las notas al pie ha sido obtenido de la [«Wikipedia,
la Enciclopedia Libre»][wikipedia]. Todo este contenido ha sido realizado por
los Colaboradores de Wikipedia y se distribuye bajo los términos de la licencia
Creative Commons Atribución CompartirIgual 3.0, compatible con la licencia de
este documento [(ver Licencia)](#licencia). Para abreviar, estas referencias
han sido marcadas con una letra uve doble en superíndice (^`W`^), por lo que un
texto como:

> —Colaboradores de Wikipedia. *Página Referenciada* \[en línea\]. Wikipedia,
> la enciclopedia libre.

Se representa de la siguiente manera, aunque tiene el mismo significado:

> —*Página Referenciada^`W`^*

[wikipedia]: https://wikipedia.org

## Licencia {-}

Este documento se publica bajo los términos de la licencia «*Creative Commons
Atribución/Reconocimiento-CompartirIgual 4.0 Internacional*» que permite la
distribución de este contenido y su edición siempre que se mantenga esta
licencia y se cite a los autores originales debidamente.

El texto completo de la licencia puede leerse en la [página oficial de Creative
Commons][license] o [al final del presente documento](#licencia-appendix).

[license]: https://creativecommons.org/licenses/by-sa/4.0/legalcode.es



# Introducción {-}

## Giacomo Tesio {-}

Giacomo Tesio es un *hacker* italiano, mitad Calabrés mitad Piamontés, lo que
le hace sentirse siempre fuera de lugar. Se presenta a sí mismo como padre,
marido y programador con 20 años de experiencia.

Se define como una persona muy política: quiere tanto a sus hijas que se niega
a abandonarlas en una distopía sin pelear todo lo posible para evitarlo. Enseña
informática a niños y niñas de forma innovadora y divertida con el objetivo de
prepararlos para las luchas que tendrán que enfrentar en el futuro y
permitirles construir un mundo mejor el día de mañana.

Desarrolla el sistema operativo Jehanne, cuyo desarrollo partió de Plan9
centrándose en la simpleza y la componibilidad.

## Sobre la traducción {-}

Es curioso que el proceso de traducción del texto refleje ideas que se plantean
en su propio contenido. Al estar creado para la Web, el texto original hace un
uso agresivo del hipertexto y, como en un libro no es posible hacer click en
enlaces y seguirlos, requiere de un proceso de conversión de todos esos enlaces
y referencias (alrededor de 80) a un formato más amigable que no rompa el ritmo
de lectura.

Principalmente, el texto original se nutre de referencias a la versión en
inglés de «Wikipedia, la Enciclopedia Libre» y otros sitios web. Se ha tomado
la decisión de sustituir las referencias originales por cortas notas al pie
dejando la posibilidad de buscar más en profundidad a quien así lo prefiera,
manteniendo referencias al contenido original o a una versión en el idioma que
nos ocupa.

Los enlaces añadidos al texto se han comprobado a fecha de edición del presente
documento, así que se obvia su fecha de consulta de las referencias para
facilitar la lectura.

Todas estas son decisiones tomadas durante el proceso de traducción y
adaptación que si a alguien le incomodaran, siempre tendría la opción de leer
el texto directamente en inglés de la fuente original.

## «*Hackers*» {-}

Antes de comenzar, debido a la confusión que ésta palabra genera en la
sociedad, principalmente por el cine y los medios de comunicación, es
obligatorio aclarar qué es un o una *hacker*.

El propio Giacomo, autor del texto que leerás a continuación, se identifica a
sí mismo como *hacker* e incluso me identifica a mí como uno. Es probable,
también, que quien nos lee sea *hacker*.

Es evidente que estamos haciendo un uso distinto de este término al que
comúnmente se hace en nuestro idioma porque, de no ser así, estaríamos
confesando que somos autores de algún delito informático. Nada más lejos de la
realidad.

Desde no hace demasiado tiempo, la Real Academia Española (RAE) define
«*hacker*», o *jáquer* en español[^hacker], como:

[^hacker]: Se usará el término en inglés en el documento.

> **jáquer**  
> Del ingl. *hacker*.
>
> 1. m. y f. Inform. pirata informático.
> 2. m. y f. Inform. Persona con grandes habilidades en el manejo de
>    computadoras que investiga un sistema informático para avisar de los
>    fallos y desarrollar técnicas de mejora.

A pesar de que la segunda de las definiciones, añadida recientemente, es más
amigable que la primera, ninguna de ellas llega a representar el fondo ni la
forma de lo que significa ser *hacker* en el contexto que aquí se trata.

El diccionario *Merryam-Webster*, en inglés, dispone de una versión más amplia
que la segunda definición de la RAE, que traducida significa algo así como
«persona experta en la programación y la resolución de problemas usando una
computadora»[^mw-hacker].

[^mw-hacker]: El texto original dice: «*an expert at programming and solving
  problems with a computer*»

La definición del *Merryam-Webster*, también indica que *hacker* es el que hace
*hack*, igual que un «pensador» es el que «piensa», así que es necesario
profundizar algo más: la palabra *hack* puede significar demasiadas cosas.
Algunos de sus significados, como un verbo, incluyen: mutilar, dar hachazos,
trocear y cortar. Sin embargo, a modo de sustantivo, la palabra *hack* tiene
otros significados, como:

> **hack**  
> *noun*
>
> - a usually creatively improvised solution to a computer hardware or
>   programming problem or limitation.
> - a clever tip or technique for doing or improving something

que, traducidos al español, dicen:

> **hack**  
> *sustantivo*
>
> - Solución, normalmente improvisada de forma creativa para resolver un
>   problema o limitación electrónica o informática
> - Técnica o truco ingenioso para resolver o mejorar algo

Partiendo de lo que ambas definiciones de *hack* proponen, la palabra *hacker*
podría significar algo así como «persona que realiza trucos ingeniosos», y,
visto el contexto en el que nos encontramos, podría añadirse, si se desea, «en
computadoras» o «con la ayuda de computadoras».

La definición podría incluir matices como que la finalidad sea la de resolver
problemas o mejorar las cosas, pero es lo bastante detallada para ver que dista
mucho de la visión de los *hackers* que la RAE recoge, que no incluye ninguna
referencia a la creatividad, la improvisación, ni el ingenio, parte fundamental
de la definición a la que Giacomo Tesio hace referencia a lo largo de su
escrito.

Esta falta de concordancia entre las definiciones en inglés y español se debe
principalmente a que el uso de la palabra *hacker* que caló en el idioma
español es el que se popularizó, tanto en países hispanohablantes como
angloparlantes, principalmente por la exposición mediática de Kevin Mitnick, un
informático que cometió una serie de delitos electrónicos durante los años 80 y
90. Por otro lado, la confusión es más que comprensible ya que ambos
significados nacen de la misma fuente, la del sector mismo.

El término *hacker* nace en el MIT (*Massachusetts Institute of Technology*),
una de las cunas de la informática, en la década de los 50, por las épicas
bromas que los estudiantes realizaban, conocidas como *hacks*, y que siguen
realizando a día de hoy[^mit-hacks]. Esta universidad es, además, parte
fundamental de la historia de la informática puesto que ha acogido, a lo largo
de su historia, a gran cantidad de personas pioneras del sector que han sido
muy influyentes tanto a nivel técnico como filosófico en los autores del
presente documento.

[^mit-hacks]:  Estas bromas, extremadamente creativas en muchos casos, han sido
  recopiladas en el sitio web <http://hacks.mit.edu/Hacks/>.

La palabra *hacker*, sin embargo, toma en ocasiones otros significados más
generales: un estilo de vida, o el conjunto de características que una persona
debe tener para considerarse *hacker*. Éstas incluyen la creatividad, la
curiosidad, el interés por la experimentación y otras muchas. A lo largo del
documento se mencionan algunas de ellas.

Ésta no es más que una de muchas palabras que los que nos dedicamos a la
programación de computadoras usamos con frecuencia. Es un caso paradigmático de
la jerga que es difícil de traducir al español. Tantas son estas palabras que
existe un glosario de argot *hacker* conocido como «*Jargon File*» que recoge
gran parte de ellas[^jargon-file]. El glosario nació en los años 70 y ha ido
creciendo desde entonces, agrupando jerga que los diferentes grupos dedicados a
la programación utilizaban en su día a día. Si bien es cierto que debido a su
procedencia aporta una visión sesgada del sector de la informática, tal es la
importancia del documento que varios diccionarios y revistas lo han usado como
fuente para incluir neologismos.

En este documento también se hace hincapié en la diferencia entre *hacker* y
delincuente informático destacando que las cualidades *hacker* son positivas e
interesantes para cualquiera.

[^jargon-file]: El «*Jargon File*» ha sido publicado como libro bajo el título
  «*The Hacker's Dictionary*», que posteriormente fue revisado y publicado como
  «*The New Hacker's Dictionary*». También está disponible en la Web de Eric S.
  Raymond, editor de «*The New Hacker's Dictionary*»:  
  <http://www.catb.org/~esr/jargon/>
