# What is informatics

This book is part of ElenQ Publishing. It's written in Pandoc's Markdown and
it's later processed using `pandoc` to generate various output formats.

# License

What is Informatics (c) by Ekaitz Zarraga and various contributors. It is
derived from [Giacomo Tesio's work][giacomo].

What is Informatics is licensed under a Creative Commons Attribution-ShareAlike
4.0 international license.

You should have received a copy of the license along with this
work.  If not, see <http://creativecommons.org/licenses/by-sa/4.0/>.

[giacomo]: http://tesio.it/2019/06/03/what-is-informatics.html
